package ru.dm_dev.a2_lesson4;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;


public class DownloadManagerService extends IntentService {
    final String TAG = "DownloadManagerService";

    public static final String DOWNLOAD_SERVICE_ACTION = "ru.dm_dev.android2.lesson4.action.DOWNLOAD";
    public static final String DOWNLOAD_SERVICE_URL = "ru.dm_dev.android2.lesson4.params.url";
    public static final String DOWNLOAD_SERVICE_FILE_NAME = "ru.dm_dev.android2.lesson4.params.file_name";

    private NotificationManager notificationManager;
    private Notification.Builder builder;
    private int NOTIFICATION = 1;

    public DownloadManagerService() {
        super("DownloadManagerService");
    }

    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "onHandleIntent");
        if (intent != null) {
            final String action = intent.getAction();
            if (DOWNLOAD_SERVICE_ACTION.equals(action)) {
                final String address = intent.getStringExtra(DOWNLOAD_SERVICE_URL);
                final String fileName = intent.getStringExtra(DOWNLOAD_SERVICE_FILE_NAME);
                handleActionDownload(address, fileName);
            }
        }
    }

    private void handleActionDownload(String address, String fileName) {
        Log.d(TAG, "Start downloading file from URL: " + address);
        startNotification();

        File path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS);
        File file = new File(path, fileName);

        InputStream input = null;
        OutputStream output = null;

        try {
            // Make sure the Pictures directory exists.
            path.mkdirs();

            URL url = new URL(address);
            URLConnection connection = url.openConnection();
            connection.connect();
            // this will be useful so that you can show a typical 0-100% progress bar
            int fileLength = connection.getContentLength();

            // download the file
            input = new BufferedInputStream(connection.getInputStream());
            output = new FileOutputStream(file);

            byte data[] = new byte[1024];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                total += count;

                updateNotification((int) (total * 100 / fileLength));

                output.write(data, 0, count);
            }

        } catch (IOException e) {
            Log.e(TAG, "Ошибка: " + e.getMessage());
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    Log.e(TAG, "Ошибка закрытия входного потока");
                }
            }

            if (output != null) {
                try {
                    output.flush();
                    output.close();
                }catch (IOException e) {
                    Log.e(TAG, "Ошибка закрытия выходного потока");
                }
            }
        }

        stopNotification(fileName);
    }

    private void startNotification() {
        notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        builder = new Notification.Builder(this);

        // Display a notification about us starting.  We put an icon in the status bar.

        // Set the info for the views that show in the notification panel.
        builder
                .setSmallIcon(R.drawable.cloud_download_dark)  // the status icon
                .setTicker(getString(R.string.ic_stat_file_download))  // the status text
                .setWhen(System.currentTimeMillis())  // the time stamp
                .setContentTitle(getString(R.string.notification_title))  // the label of the entry
                .setContentText(getString(R.string.notification_text_progress))  // the contents of the entry
                .setProgress(0, 0, true);   // indeterminate length

        notificationManager.notify(NOTIFICATION, builder.build());
    }

    private void updateNotification(int percent) {
        builder.setProgress(100, percent, false);
        notificationManager.notify(NOTIFICATION, builder.build());

    }

    private void stopNotification(String fileName) {
        builder
                .setTicker(getString(R.string.notification_text_complete))  // the status text
                .setWhen(System.currentTimeMillis())  // the time stamp
                .setContentTitle(getString(R.string.notification_text_complete))  // the label of the entry
                .setContentText(getString(R.string.notification_text_file_name) + fileName)  // the contents of the entry
                .setProgress(0, 0, false);   // removes the progress bar

        notificationManager.notify(NOTIFICATION, builder.build());

        // Cancel the persistent notification.
        //mNM.cancel(NOTIFICATION);
    }

    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }
}
