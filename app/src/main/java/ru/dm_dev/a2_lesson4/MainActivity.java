package ru.dm_dev.a2_lesson4;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "MainActivity";
    ImageView imageView = null;
    EditText textAddress = null;
    EditText textFileName = null;
    Button btnLoad = null;

    private static final int REQUEST_PERMISSIONS_AND_WRITE_EXTERNAL_STORAGE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnLoad = (Button)findViewById(R.id.btnLoad);
        btnLoad.setOnClickListener(this);
        imageView = (ImageView)findViewById(R.id.imageView);
        textAddress = (EditText)findViewById(R.id.textAddress);
        textFileName = (EditText)findViewById(R.id.textFileName);
    }

    public void doDownload() {
        Intent intent = new Intent(this, DownloadManagerService.class);
        intent.setAction(DownloadManagerService.DOWNLOAD_SERVICE_ACTION);
        intent.putExtra(DownloadManagerService.DOWNLOAD_SERVICE_URL, textAddress.getText().toString());
        intent.putExtra(DownloadManagerService.DOWNLOAD_SERVICE_FILE_NAME,  textFileName.getText().toString());
        startService(intent);
    }


    @Override
    public void onClick(View v) {
        if (btnLoad.equals(v)) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                doDownload();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_PERMISSIONS_AND_WRITE_EXTERNAL_STORAGE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSIONS_AND_WRITE_EXTERNAL_STORAGE
                && grantResults.length == 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            doDownload();
        }
    }
}
