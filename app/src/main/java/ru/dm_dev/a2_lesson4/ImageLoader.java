package ru.dm_dev.a2_lesson4;

import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ImageLoader extends Service {

    public static final String PARAM_NAME = "IMAGE_URL";
    private static final String TAG = "ImageLoader";

    private String mImageUrl = null;

    private final IBinder mBinder = new ServiceBinder();

//    private final ImageLoaderAsyncTask imageLoader = new ImageLoaderAsyncTask();

    private Bitmap bitmap = null;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.i(TAG,"Started");

		/* Get data from intent */
        if (intent != null) {
            mImageUrl = intent.getStringExtra(PARAM_NAME);

			/* Checking data from intent and show it */
            if (mImageUrl != null) {
                Log.i(TAG,"Url: " + mImageUrl);
                Toast.makeText(this, mImageUrl, Toast.LENGTH_SHORT).show();
            }
        }

		/* Invoke a parent method and return value */
        return super.onStartCommand(intent, flags, startId);
    }

    public void execute() {
        try {
            byte[] bitmapBytes = getUrlBytes(mImageUrl);
            bitmap = BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length);
            Log.i(TAG, "Image loaded and decoded");
        } catch (IOException e) {
            Log.e(TAG, "Error downloading image. " + e.getMessage().toString());
        }
        finally {
            OnFinishListener l = getOnFinishListener();
            if (l != null) l.onFinish(bitmap);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class ServiceBinder extends Binder {

        public ImageLoader getService() {
            return ImageLoader.this;
        }
    }

    public byte[] getUrlBytes(String urlSpec) throws IOException {
        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(connection.getResponseMessage() + ": with " + urlSpec);
            }
            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            return out.toByteArray();
        } finally {
            connection.disconnect();
        }
    }

    public interface OnFinishListener {
        void onFinish(Bitmap bitmap);
    }

    private OnFinishListener mOnFinishListener = null;

    public OnFinishListener getOnFinishListener() {
        return mOnFinishListener;
    }

    public void setOnFinishListener(OnFinishListener l) {
        mOnFinishListener = l;
    }
}
